package org.rada7.prototype.threads;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class ThreadsService {
    @Resource
    private ThreadsDao threadsDao;

    public void updateThreadMeta(Integer id, String title, String content) {
        threadsDao.updateThreadMeta(id, title, content);
    }

    public void deleteThread(Integer id) {
        threadsDao.deleteThread(id);
    }

    public Thread createThread(String title, String content, Integer userId) {
        Integer lastId = threadsDao.createThread(title, content, userId);
        return threadsDao.getThread(lastId);
    }
}
