package org.rada7.prototype.threads;

import org.rada7.prototype.config.BaseDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.io.StringReader;
import java.sql.*;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

@Repository
public class ThreadsDao extends BaseDao {
    private static final Logger logger = LoggerFactory.getLogger(ThreadsDao.class);

    public void updateThreadMeta(Integer id, String title, String content) {
        String sql = "UPDATE thread SET TITLE = ?, CONTENT = ? WHERE id = ?";
        getJdbcTemplate().update(sql, title, content, id);
    }

    public void deleteThread(Integer id) {
        String sql = "DELETE FROM thread WHERE id = ?";
        getJdbcTemplate().update(sql, id);
    }

    public Thread getThread(Integer id) {
        String sql = "SELECT * FROM thread WHERE id = ?";
        try {
            return (Thread) getJdbcTemplate().queryForObject(sql, new Object[]{id}, new BeanPropertyRowMapper(Thread.class));
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    public Integer createThread(String title, String content, Integer userId) {
        String sql = "INSERT INTO thread (TITLE,CONTENT,USER_ID,CREATED_AT) VALUES (?,?,?,?)";
        LocalDateTime localDateTime = LocalDateTime.now();
        Instant instant = localDateTime.atZone(ZoneId.systemDefault()).toInstant();
        Date date = Date.from(instant);
        KeyHolder keyHolder = new GeneratedKeyHolder();
        getJdbcTemplate().update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                PreparedStatement statement = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                statement.setString(1, title);
                statement.setCharacterStream(2, new StringReader(content));
                statement.setInt(3, userId);
                statement.setTimestamp(4, new Timestamp(date.getTime()));
                return statement;
            }
        }, keyHolder);
        return keyHolder.getKey().intValue();
    }
}
