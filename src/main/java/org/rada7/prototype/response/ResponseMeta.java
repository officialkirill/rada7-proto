package org.rada7.prototype.response;

public class ResponseMeta {
    private Integer code;

    public ResponseMeta() {
    }

    public ResponseMeta(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}
