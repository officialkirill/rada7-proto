package org.rada7.prototype.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.http.HttpStatus;

@SuppressWarnings("unchecked")
public class SuccessResponse<E> {
    @JsonIgnore
    private E e;
    private Object data;
    private ResponseMeta meta;

    public static SuccessResponse createResponse(Object data) {
        SuccessResponse response = new SuccessResponse<>();
        response.setData(data);
        response.setE(data.getClass());
        response.setMeta(new ResponseMeta(HttpStatus.OK.value()));
        return response;
    }


    public static SuccessResponse createResponse(Object data, HttpStatus httpStatus) {
        SuccessResponse response = new SuccessResponse<>();
        response.setData(data);
        response.setE(data.getClass());
        response.setMeta(new ResponseMeta(httpStatus.value()));
        return response;
    }

    public E getE() {
        return e;
    }

    public void setE(E e) {
        this.e = e;
    }

    public ResponseMeta getMeta() {
        return meta;
    }

    public void setMeta(ResponseMeta meta) {
        this.meta = meta;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
