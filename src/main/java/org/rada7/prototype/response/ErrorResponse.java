package org.rada7.prototype.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;

public class ErrorResponse {

    private List<String> errors;
    @JsonProperty("code")
    private Integer httpCode;
    @JsonIgnore
    private HttpStatus httpStatus;
    @JsonIgnore
    private Exception exception;
    @JsonProperty("exception")
    @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
    private String exceptionName;

    public ErrorResponse() {
    }

    public ErrorResponse(List<String> errors, HttpStatus httpStatus) {
        this.errors = errors;
        this.httpStatus = httpStatus;
        if (httpStatus != null) httpCode = httpStatus.value();
    }

    public ErrorResponse(String error, HttpStatus httpStatus) {
        addError(error);
        this.httpStatus = httpStatus;
        if (httpStatus != null) httpCode = httpStatus.value();
    }

    public ErrorResponse(List<String> errors, HttpStatus httpStatus, Exception exception) {
        this.errors = errors;
        this.httpStatus = httpStatus;
        if (httpStatus != null) httpCode = httpStatus.value();
        if (exception != null) exceptionName = exception.getClass().getCanonicalName();
    }

    public ErrorResponse(String error, HttpStatus httpStatus, Exception exception) {
        addError(error);
        this.httpStatus = httpStatus;
        if (httpStatus != null) httpCode = httpStatus.value();
        if (exception != null) exceptionName = exception.getClass().getCanonicalName();
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    public void addError(String error) {
        if (errors == null) errors = new ArrayList<>();
        errors.add(error);
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }

    public Integer getHttpCode() {
        return httpCode;
    }
}
