package org.rada7.prototype.articles;

import org.jsondoc.core.annotation.Api;
import org.jsondoc.core.annotation.ApiAuthNone;
import org.jsondoc.core.annotation.ApiBodyObject;
import org.jsondoc.core.annotation.ApiMethod;
import org.jsondoc.core.annotation.ApiResponseObject;
import org.jsondoc.core.annotation.ApiVersion;
import org.rada7.prototype.exceptions.ContentNotFoundException;
import org.rada7.prototype.response.SuccessResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/articles")
@ApiVersion(since = "1.0")
@ApiAuthNone
@Api(name = "Articles endpoints", description = "Retrieve articles, all or specify by id. You can also create new articles and edit existing.")
@SuppressWarnings("unchecked")
public class ArticlesController {

    private static final Logger logger = LoggerFactory.getLogger(ArticlesController.class);

    @Resource
    private ArticlesService articlesService;

    @Resource
    private MessageSourceAccessor messageSourceAccessor;

    @ApiMethod(description = "Get a list of all available articles", produces = "application/json")
    @RequestMapping(method = RequestMethod.GET)
    @ApiResponseObject(clazz = Article.class)
    public SuccessResponse<List<Article>> getArticles() {
        // todo permissions
        return SuccessResponse.createResponse(articlesService.getArticles(/* boolean value */));
    }

    @ApiMethod(description = "Create a new article", produces = "application/json", consumes = "application/json")
    @ApiResponseObject(clazz = Article.class)
    @ApiBodyObject(clazz = Article.class)
    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public SuccessResponse<Article> createArticle(@Valid @RequestBody Article article) {
        // todo user permissions
        return SuccessResponse.createResponse(articlesService.createArticle(article, 0), HttpStatus.CREATED); // todo change user id
    }

    @ApiMethod(description = "Get a specific article based on id", produces = "application/json")
    @ApiResponseObject(clazz = Article.class)
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public SuccessResponse<Article> getArticle(@PathVariable Integer id) {
        Article article = articlesService.getArticle(id);
        // todo permission
        if (article == null)
            throw new ContentNotFoundException(messageSourceAccessor.getMessage("exceptions.article.not_found"));
        return SuccessResponse.createResponse(article);
    }

    @ApiMethod(description = "Edit a specific article", produces = "application/json", consumes = "application/json")
    @ApiResponseObject(clazz = Article.class)
    @ApiBodyObject(clazz = Article.class)
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public SuccessResponse<Article> editArticle(@RequestBody @Valid Article article,
                                                @PathVariable Integer id) {
        logger.debug("input article: {}", article);
        Article current = articlesService.getArticle(id);
        // todo permission check
        if (article == null)
            throw new ContentNotFoundException(messageSourceAccessor.getMessage("exceptions.article.not_found"));
        return SuccessResponse.createResponse(articlesService.editArticle(current, article));
    }

    @ApiMethod(description = "Delete a specific article", produces = "application/json", consumes = "application/json")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public
    @ApiResponseObject
    void deleteArticle(@PathVariable Integer id) {
        Article article = articlesService.getArticle(id);
        // todo permission check
        if (article == null)
            throw new ContentNotFoundException(messageSourceAccessor.getMessage("exceptions.article.not_found"));
        articlesService.deleteArticle(article);
    }
}
