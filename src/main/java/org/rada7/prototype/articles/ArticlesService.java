package org.rada7.prototype.articles;

import org.rada7.prototype.threads.Thread;
import org.rada7.prototype.threads.ThreadsService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ArticlesService {
    @Resource
    private ArticlesDao articlesDao;

    @Resource
    private ThreadsService threadsService;

    public List<Article> getArticles() {
        return articlesDao.getArticles(true);
    }

    public List<Article> getArticles(Boolean showHidden) {
        return articlesDao.getArticles(showHidden);
    }

    public Article getArticle(Integer id) {
        return articlesDao.getArticle(id);
    }

    public Article editArticle(Article current, Article article) {
        article.setId(current.getId());
        article.setThreadId(current.getThreadId());
        articlesDao.editArticle(article);
        threadsService.updateThreadMeta(current.getThreadId(), article.getTitle(), article.getContent());
        return getArticle(current.getId());
    }

    public void deleteArticle(Article article) {
        articlesDao.deleteArticle(article.getId());
        threadsService.deleteThread(article.getThreadId());
    }

    public Article createArticle(Article article, Integer userId) {
        Thread thread = threadsService.createThread(article.getTitle(), article.getContent(), userId);
        article.setThreadId(thread.getId());
        Integer lastId = articlesDao.createArticle(article);
        return articlesDao.getArticle(lastId);
    }
}
