package org.rada7.prototype.articles;

import org.rada7.prototype.config.BaseDao;
import org.rada7.prototype.config.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.io.StringReader;
import java.sql.*;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

@Repository
public class ArticlesDao extends BaseDao {
    private static final Logger logger = LoggerFactory.getLogger(ArticlesDao.class);

    public List<Article> getArticles(Boolean showHidden) {
        String where = showHidden ? "" : "WHERE THREAD.HIDDEN != TRUE\n";

        String sql = "SELECT\n" +
                "    ARTICLE.*,\n" +
                "    THREAD.TITLE,\n" +
                "    THREAD.CONTENT,\n" +
                "    THREAD.HIDDEN\n" +
                "FROM ARTICLE\n" +
                "    JOIN THREAD ON ARTICLE.THREAD_ID = THREAD.ID\n" +
                where +
                "ORDER BY ARTICLE.PUBLISH_ON DESC";

        logger.debug("{}", sql);

        return getJdbcTemplate().query(sql, new BeanPropertyRowMapper(Article.class));
    }

    public Article getArticle(Integer id) {
        String sql = "SELECT ARTICLE.*, THREAD.TITLE, THREAD.CONTENT, THREAD.HIDDEN FROM ARTICLE JOIN THREAD ON THREAD.ID = ARTICLE.THREAD_ID WHERE ARTICLE.ID = ?";
        logger.debug("{}", sql);
        try {
            return (Article) getJdbcTemplate().queryForObject(sql, new Object[]{id}, new BeanPropertyRowMapper(Article.class));
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    public void editArticle(Article article) {
        String sql = "UPDATE article SET intro = ?, modified_at = ? WHERE id = ?";
        LocalDateTime modifiedAt = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(Config.FULL_TIMESTAMP_FORMAT);
        getJdbcTemplate().update(sql, article.getIntro(), modifiedAt.format(formatter), article.getId());
    }

    public void deleteArticle(Integer id) {
        String sql = "DELETE FROM article WHERE id = ?";
        getJdbcTemplate().update(sql, id);
    }

    public Integer createArticle(Article article) {
        String sql = "INSERT INTO article (intro, thread_id, created_at) VALUES (?,?,?)";
        LocalDateTime localDateTime = LocalDateTime.now();
        Instant instant = localDateTime.atZone(ZoneId.systemDefault()).toInstant();
        Date date = Date.from(instant);
        KeyHolder keyHolder = new GeneratedKeyHolder();
        getJdbcTemplate().update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                PreparedStatement statement = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                statement.setCharacterStream(1, new StringReader(article.getIntro()));
                statement.setInt(2, article.getThreadId());
                statement.setTimestamp(3, new Timestamp(date.getTime()));
                return statement;
            }
        }, keyHolder);
        return keyHolder.getKey().intValue();
    }
}
