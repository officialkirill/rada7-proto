package org.rada7.prototype.articles;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.jsondoc.core.annotation.ApiObject;
import org.jsondoc.core.annotation.ApiObjectField;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@ApiObject
public class Article {
    @ApiObjectField
    private Integer id;
    @ApiObjectField
    private Integer threadId;
    @Size(min = 3, max = 100)
    @NotBlank
    @ApiObjectField
    private String title;
    @Size(min = 10)
    @NotBlank
    @ApiObjectField
    private String intro;
    @Size(min = 10)
    @NotBlank
    @ApiObjectField
    private String content;
    @ApiObjectField
    private Date createdAt;
    @JsonIgnore
    private Date modifiedAt;
    @ApiObjectField
    private Date publishOn;
    @ApiObjectField
    private String authorNames;
    @ApiObjectField
    private Boolean hidden;
    @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
    private List<Author> authors;

    public Boolean getHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getThreadId() {
        return threadId;
    }

    public void setThreadId(Integer threadId) {
        this.threadId = threadId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getModifiedAt() {
        return modifiedAt;
    }

    public void setModifiedAt(Date modifiedAt) {
        this.modifiedAt = modifiedAt;
    }

    public Date getPublishOn() {
        return publishOn;
    }

    public void setPublishOn(Date publishOn) {
        this.publishOn = publishOn;
    }

    public String getAuthorNames() {
        return authorNames;
    }

    public void setAuthorNames(String authorNames) {
        this.authorNames = authorNames;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}
