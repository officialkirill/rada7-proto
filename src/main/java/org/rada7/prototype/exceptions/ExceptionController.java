package org.rada7.prototype.exceptions;

import org.rada7.prototype.response.ErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * java exception names and values for demo purpose only
 */
@ControllerAdvice
@RestController
public class ExceptionController {
    private static final Logger logger = LoggerFactory.getLogger(ExceptionController.class);

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public ErrorResponse handleServerExceptions(Exception exception) throws Exception {
        return new ErrorResponse(exception.getLocalizedMessage(), HttpStatus.INTERNAL_SERVER_ERROR, exception);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = {MethodArgumentNotValidException.class, InputValidationException.class})
    public ErrorResponse handleInputExceptions(MethodArgumentNotValidException ex) {
        HttpStatus httpStatus = HttpStatus.BAD_REQUEST;
        List<String> errors = new ArrayList<>();
        for (ObjectError error : ex.getBindingResult().getAllErrors()) {
            errors.add(error.getCodes()[0] + ": " + error.getDefaultMessage());
        }
        return new ErrorResponse(errors, httpStatus);
    }

    @ResponseStatus(HttpStatus.GONE)
    @ExceptionHandler(ContentNotFoundException.class)
    public ErrorResponse handleContentNotFoundException(Exception exception) {
        return new ErrorResponse(exception.getLocalizedMessage(), HttpStatus.GONE);
    }

    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ExceptionHandler(PermissionDeniedException.class)
    public ErrorResponse handlePermissionDeniedException(Exception exception) {
        return new ErrorResponse(exception.getLocalizedMessage(), HttpStatus.FORBIDDEN);
    }

    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    @ExceptionHandler({UnsupportedOperationException.class, HttpRequestMethodNotSupportedException.class})
    public ErrorResponse handleHttpRequestMethodNotSupportedException(Exception exception) {
        return new ErrorResponse(exception.getLocalizedMessage(), HttpStatus.METHOD_NOT_ALLOWED);
    }
}
