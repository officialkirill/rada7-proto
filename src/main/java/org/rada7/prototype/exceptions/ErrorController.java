package org.rada7.prototype.exceptions;

import org.rada7.prototype.response.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/error")
public class ErrorController {

    @RequestMapping("/401")
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public ErrorResponse handleUnauthorized() {
        return new ErrorResponse("UNAUTHORIZED", HttpStatus.UNAUTHORIZED);
    }

    @RequestMapping("/403")
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public ErrorResponse handleForbiddenError() {
        return new ErrorResponse("forbidden", HttpStatus.FORBIDDEN);
    }

    @RequestMapping("/404")
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorResponse handleNotFound() {
        return new ErrorResponse("not found", HttpStatus.NOT_FOUND);
    }
}
