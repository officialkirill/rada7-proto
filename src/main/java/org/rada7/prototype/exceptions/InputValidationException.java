package org.rada7.prototype.exceptions;

import org.springframework.validation.ObjectError;

import java.util.List;

public class InputValidationException extends RuntimeException {
    public InputValidationException() {
        super();
    }

    public InputValidationException(String message) {
        super(message);
    }

    public InputValidationException(List<ObjectError> errors) {
        StringBuilder message = new StringBuilder();
        for (ObjectError item : errors) {
            message.append(item.toString());
        }
        throw new InputValidationException(message.toString());
    }
}
