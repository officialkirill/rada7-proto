package org.rada7.prototype.exceptions;

public class ContentNotFoundException extends RuntimeException {
    public ContentNotFoundException() {
    }

    public ContentNotFoundException(String message) {
        super(message);
    }
}
