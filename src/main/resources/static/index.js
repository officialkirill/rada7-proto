function ErrorHandler(errorResponse, $scope) {
    $scope.generalErrors = errorResponse.errors;
    $scope.errorCode = errorResponse.code;
}

function ContentNotFoundHandler(errorResponse, $scope) {
    $scope.notFoundErrors = errorResponse.errors;
    $scope.errorCode = errorResponse.code;
}

angular.module('Rada7', ['ngRoute', 'duScroll'])
    .config(function ($routeProvider, $locationProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'list.html',
                controller: 'ListController'
            })
            .when('/article/create', {
                templateUrl: 'create.html',
                controller: 'CreateController'
            })
            .when('/article/:id', {
                templateUrl: 'edit.html',
                controller: 'EditController'
            })
            .when('/docs', {
                templateUrl: 'docs.html'
            });
    })
    .controller('AppController', function ($scope, $document) {
        $scope.scrollToId = function (id) {
            console.log(id);
            $document.scrollTo(id, 30);
        }
    })
    .controller('ListController', function ($scope, $http) {
        // get a list of all articles from REST service endpoint
        $http.get('/articles').success(function (response) {
            $scope.articleList = response.data;
        });
    })
    .controller('CreateController', function ($scope, $http, $location) {
        $scope.article = {};

        $scope.create = function () {
            //  send a json article object to REST endpoint for saving the article
            $http.post('articles', $scope.article)
                .success(function () {
                    $location.path('/');
                })
                .error(function (response) {
                    ErrorHandler(response, $scope)
                });
        }
    })
    .controller('EditController', function ($scope, $http, $location, $routeParams) {
        var articleId = $routeParams.id;
        // get a specific article from REST service endpoint
        $http.get('/articles/' + articleId)
            .success(function (response) {
                $scope.article = response.data;
            })
            .error(function (response) {
                ContentNotFoundHandler(response, $scope)
            });

        $scope.update = function () {
            // edit a specific article, sends an article object to the REST service endpoint
            $http.put('/articles/' + articleId, $scope.article)
                .success(function () {
                    $location.path('/');
                })
                .error(function (response) {
                    ErrorHandler(response, $scope)
                });
        };

        // delete a specific article with the REST service endpoint
        $scope.delete = function (article) {
            $http.delete('/articles/' + article.id)
                .success(function () {
                    $location.path('/');
                })
                .error(function (response) {
                    ErrorHandler(response, $scope)
                });
        };
    });