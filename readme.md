# Rada7 prototype: API + AngularJS single page application client example

## Environment setup
* Open your terminal/command line (cmd.exe in Windows)
* Make sure you have Java 8 SE (JDK) installed. You can download it here: http://www.oracle.com/technetwork/java/javase/downloads/index.html
* To verify Java is installed, type `java -version` in your terminal.
  Additionally, type in `echo %JAVA_HOME%` (Windows) or `echo $JAVA_HOME` (Linux/Mac OSX),
  which should return something like `c:\Program Files\Java\jdk1.8.0_40`
 
## Starting the server
* Make sure you don't have anything running on port 8080
* Type `gradlew` (Windows) or `sh gradlew` (Linux/Mac OSX) in your terminal.
  The initial run might take some time to download all the project dependencies.
  The server is good to go once you see something like
  ```Started Application in 4.629 seconds (JVM running for 5.057)
  > Building 94% > :bootRun```
* Go to http://localhost:8080

## Shutting the server down
* Press Ctrl+C twice in the terminal